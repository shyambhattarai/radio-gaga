package com.projects.home.radiogaga;

        import android.bluetooth.BluetoothAdapter;
        import android.content.Intent;
        import android.graphics.Color;
        import android.media.AudioManager;
        import android.media.MediaPlayer;
        import android.os.AsyncTask;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    Button btn_radioNepal;
    Button btn_imageFM;
    Button btn_kantipurFM;

    MediaPlayer mediaPlayer;

    String streamRadioNepal = "http://www.radionepal.gov.np:40100/stream";
    String streamImageFM = "http://streaming.hamropatro.com:8631/;stream.mp3";
    String streamKantipurFM = "http://broadcast.radiokantipur.com:7248/stream";
    boolean prepared = false, pauseRadioNepal = false, pauseImageFM = false, pauseKantipurFM = false;
    boolean isAnyMediaPlayerActive = false;

    private final static int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_radioNepal = (Button) findViewById(R.id.btn_radioNepal);
        btn_radioNepal.setEnabled(false);
        btn_radioNepal.setText("LOADING");

        btn_imageFM = (Button) findViewById(R.id.btn_imageFM);
        btn_imageFM.setEnabled(false);
        btn_imageFM.setText("LOADING");


        btn_kantipurFM = (Button) findViewById(R.id.btn_kantipurFM);
        btn_kantipurFM.setEnabled(false);
        btn_kantipurFM.setText("LOADING");

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }

        new PlayerTaskRadioNepal().execute(streamRadioNepal);
        new PlayerTaskImageFM().execute(streamImageFM);
        new PlayerTaskKantipurFM().execute(streamKantipurFM);


        final Button btn_radioNepalLogicBtn = (Button) findViewById(R.id.btn_radioNepalLogic);
        final Button btn_kantipurFMLogicBtn = (Button) findViewById(R.id.btn_kantipurFMLogic);
        final Button btn_imageFMLogicBtn = (Button) findViewById(R.id.btn_imageFMLogic);

        btn_radioNepalLogicBtn.setOnClickListener(new View.OnClickListener()
        {
            private String[] parametersRadioNepal = {streamRadioNepal, String.valueOf(btn_radioNepal.getId())};

            @Override
            public void onClick(View view)
            {
                if(pauseRadioNepal)
                {
                    pauseRadioNepal = false;
                    mediaPlayer.release();
                    btn_radioNepal.setText("RADIO NEPAL");
                    btn_radioNepal.setBackgroundColor(Color.GREEN);
                    isAnyMediaPlayerActive = false;

                    btn_imageFM.setEnabled(true);
                    btn_kantipurFM.setEnabled(true);
                }
                else
                {
                    pauseRadioNepal = true;

                    if(isAnyMediaPlayerActive)
                    {
                        mediaPlayer.stop();
                    }

                    btn_radioNepal.setEnabled(false);

                    new LoadingButtonTask().execute(parametersRadioNepal);
                    btn_imageFM.setEnabled(false);
                    btn_kantipurFM.setEnabled(false);
                }
            }
        });

        btn_imageFMLogicBtn.setOnClickListener(new View.OnClickListener()
        {
            private String[] parametersImageFM = {streamImageFM, String.valueOf(btn_imageFM.getId())};

            @Override
            public void onClick(View view)
            {
                if(pauseImageFM)
                {
                    pauseImageFM = false;
                    mediaPlayer.release();
                    btn_imageFM.setText("IMAGE FM 97.9");
                    isAnyMediaPlayerActive = false;
                    btn_imageFM.setBackgroundColor(Color.GREEN);

                    btn_radioNepal.setEnabled(true);
                    btn_kantipurFM.setEnabled(true);
                }
                else
                {
                    pauseImageFM = true;

                    btn_imageFM.setEnabled(false);

                    new LoadingButtonTask().execute(parametersImageFM);

                    btn_kantipurFM.setEnabled(false);
                    btn_radioNepal.setEnabled(false);
                }
            }
        });


        btn_kantipurFMLogicBtn.setOnClickListener(new View.OnClickListener()
        {
            private String[] parametersKantipurFM = {streamKantipurFM, String.valueOf(btn_kantipurFM.getId())};

            @Override
            public void onClick(View view)
            {
                if(pauseKantipurFM)
                {
                    pauseKantipurFM = false;

                    mediaPlayer.release();
                    btn_kantipurFM.setText("Kantipur FM 96.1");
                    isAnyMediaPlayerActive = false;
                    btn_kantipurFM.setBackgroundColor(Color.GREEN);

                    btn_imageFM.setEnabled(true);
                    btn_radioNepal.setEnabled(true);
                }
                else
                {
                    pauseKantipurFM = true;

                    btn_kantipurFM.setEnabled(false);

                    new LoadingButtonTask().execute(parametersKantipurFM);
                    btn_imageFM.setEnabled(false);
                    btn_radioNepal.setEnabled(false);
                }
            }
        });


        btn_radioNepal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                btn_radioNepal.setText("LOADING");
                btn_radioNepal.setBackgroundColor(Color.YELLOW);
                btn_radioNepalLogicBtn.performClick();
            }

        });

        btn_imageFM.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                btn_imageFM.setText("LOADING");
                btn_imageFM.setBackgroundColor(Color.YELLOW);
                btn_imageFMLogicBtn.performClick();
            }
        });

        btn_kantipurFM.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                btn_kantipurFM.setText("LOADING");
                btn_kantipurFM.setBackgroundColor(Color.YELLOW);
                btn_kantipurFMLogicBtn.performClick();
            }
        });

    }

    class PlayerTaskRadioNepal extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(String... strings)
        {
            try
            {
                prepared = true;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            super.onPostExecute(aBoolean);
            btn_radioNepal.setEnabled(true);
            btn_radioNepal.setText("RADIO NEPAL");
            btn_radioNepal.setBackgroundColor(Color.GREEN);
        }
    }

    class PlayerTaskImageFM extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(String... strings)
        {

            try
            {
                /*mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.prepare();*/
                prepared = true;

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            super.onPostExecute(aBoolean);

            btn_imageFM.setEnabled(true);
            btn_imageFM.setText("IMAGE FM 97.9");
            btn_imageFM.setBackgroundColor(Color.GREEN);
        }
    }

    class PlayerTaskKantipurFM extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(String... strings)
        {

            try {
                /*mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.prepare();*/
                prepared = true;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            super.onPostExecute(aBoolean);
            btn_kantipurFM.setEnabled(true);
            btn_kantipurFM.setText("Kantipur FM 96.1");
            btn_kantipurFM.setBackgroundColor(Color.GREEN);
        }
    }

/*    private void LoadingButton(Button targetBtn)
    {
        targetBtn.setText("LOADING");
        targetBtn.setBackgroundColor(Color.YELLOW);
    }
*/
    class LoadingButtonTask extends AsyncTask<String, Void, Boolean>
    {
        String targetBtnPassed = "";


        @Override
        protected Boolean doInBackground(String... strings)
        {
            try
            {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(strings[0]);
                targetBtnPassed = strings[1];
                mediaPlayer.prepare();
                prepared = true;
            }

            catch(Exception e)
            {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean)
        {
            Button targetButton = (Button) findViewById(Integer.parseInt(targetBtnPassed));

            super.onPostExecute(aBoolean);
            targetButton.setEnabled(true);
            isAnyMediaPlayerActive = true;
            mediaPlayer.start();
            targetButton.setText("PLAYING");
            targetButton.setBackgroundColor(Color.RED);
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        /*if(pauseRadioNepal)
        {
            mediaPlayer.pause();
        }*/
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        /*if(pauseRadioNepal)
        {
            mediaPlayer.start();
        }*/
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        /*if(prepared)
        {
            mediaPlayer.start();
        }*/

    }
}
